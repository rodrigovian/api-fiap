require('dotenv').config();

const responseErrors = require('./lib/controller/responseErrors.js');
const responseSuccess = require('./lib/controller/responseSuccess');
const Autenticacao = require('./lib/controller/autenticacao');

const jwt = require('jsonwebtoken');

const restify = require('restify');

const server = restify.createServer({
    name: process.env.API_NOME,
    url: process.env.API_URL
});

const mongoose = require('mongoose');
const conn = 'mongodb://localhost/apidozodiaco';
// const conn = process.env.API_MONGODB;
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify : false
}
const cavaleiroSchema = new mongoose.Schema({
    nome: String,
    descricao: String,
    patente: String,
    ataques: Array,
    deus: String,
    versionKey: false
})

const usuarioSchema = new mongoose.Schema({
    nome: String,
    usuario: String,
    senha: {type: String, select: false},
    versionKey: false
})

const cavaleiros = mongoose.model('cavaleiros',cavaleiroSchema);
const usuarios = mongoose.model('usuarios', usuarioSchema);

/** O Plugin bodyParser se faz necessário para receber json via post */
server.use(restify.plugins.bodyParser())
/** O Plugin queryParser se faz necessario para converter a query http em objeto */
server.use(restify.plugins.queryParser());

/** Conexão com mongodb */
mongoose.connect(conn, options);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

function verificarJWT(req, res, next){
    let validarErros = new responseErrors;
    let retornaSucesso = new responseSuccess;
    let token = req.headers['x-zodiaco-token'];    
    
    if (!token){
        let nenhumTokenFornecido = validarErros.nenhumTokenFornecido('token');
        return res.send(nenhumTokenFornecido.status, nenhumTokenFornecido);  
    }     
    
    jwt.verify(token, process.env.API_TOKEN, function(err, decoded) {    
        let tokenInvalido = validarErros.tokenInvalido('token');
        if (err) return res.send(tokenInvalido.status, tokenInvalido);
        /** Id da sessão */
        req.userId = decoded.id;   
        next();
    });
}  



/**
 * @swagger
 * /login:
 *   post:
 *     tags:
 *       - login
 *     summary: "Efetua login e retorna o x-zodiaco-token"
 *     description: ""
 *     parameters: 
 *       - in: "body"
 *         name: "body"
 *         description: "Parametro obrigatórios para efetuar o login"
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *             usuario:
 *               type: string
 *               description: Nome de usuário
 *             senha:
 *               type: string
 *               description: Senha de acesso
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Retorna x-zodiaco-token que deverá ser utilizado a cada requisição
 *         schema:
 *           type: object
 *           properties:
 *             success:
 *               type: boolean
 *               description: retorna true quando login é retornado com sucesso
 *             message:
 *               type: string
 *               description: Mensgem informativa
 *             data:
 *               type: object
 *               description: Objeto contendo a chave x-zodiaco-token
 *               properties:
 *                 x-zodiaco-token: 
 *                   type: object
 *                   description: Contem o token que deverá ser utilizado
 *                   properties:
 *                     token:
 *                       type: string
 *                       description: String que deverá ser enviado no header x-zodiaco-token
 */
server.post('/login',function(req,res,next){    
    let validarErros = new responseErrors;
    let retornaSucesso = new responseSuccess;

    if(req.body == undefined){
        let credenciaisNaoInformadas = validarErros.credenciaisNaoInformadas('/login');
        return res.send(credenciaisNaoInformadas.status, credenciaisNaoInformadas);
    }
        

    let usuario = req.body.usuario;
    let senha  = req.body.senha;
         
    usuarios.find({usuario: usuario, senha: senha},function(err, login){
        if (err) return console.error(err)
        if(login.length == 0){
            let falhaLogin = validarErros.falhaNaAutenticacao('/login')
            res.send(falhaLogin.status, falhaLogin); 
        }else{
            if (login[0]._id != undefined && login[0]._id != null){
                let autenticacao = new Autenticacao;
                let token = autenticacao.gerarNovotoken(login[0]._id,login[0].nome,usuario,senha);
                let returnToken = retornaSucesso.tokenGeradoComSucesso(token)
                res.send(200, returnToken);
            }else{
                /** Falha no login */
                let falhaLogin = validarErros.falhaNaAutenticacao('/login')
                res.send(falhaLogin.status, falhaLogin); 
            }
        }
        
    })
})

/**
 * @swagger
 * /cavaleiro:
 *   post:
 *     tags:
 *       - cavaleiro
 *     summary: "Usado para efetuar o cadastro de um novo cavaleiro"
 *     description: ""
 *     parameters: 
 *       - name: x-zodiaco-token
 *         in: header
 *         description: ""
 *         required: true
 *         type: string 
 *       - in: "body"
 *         name: "body"
 *         description: "Parametro obrigatórios para cadastro"
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *             nome:
 *               type: string
 *               description: Nome de usuário
 *             patente:
 *               type: string
 *               description: bronze, prata, ouro, etc
 *             descricao:
 *               type: string
 *               description: Senha de acesso
 *             ataques:
 *               type: array
 *               items:
 *                 type: string
 *               description: Ataques utilizado pelo cavaleiro
 *             deus:
 *               type: string
 *               description: Deus protetor do cavaleiro
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Retorna informações cadastradas
 *         schema:
 *           type: object
 *           properties:
 *             success:
 *               type: boolean
 *               description: retorna true quando login é retornado com sucesso
 *             message:
 *               type: string
 *               description: Mensgem informativa
 *             data:
 *               type: object
 *               description: Objeto contendo a chave x-zodiaco-token
 *               properties:
 *                 nome: 
 *                   type: string
 *                 patente:
 *                   type: string
 *                 descricao: 
 *                   type: string
 *                 ataques:
 *                   type: array
 *                   items:
 *                     type: string
 *                 deus: 
 *                   type: string
 */
server.post('/cavaleiro', verificarJWT, function(req,res,next){
    let validarErros = new responseErrors;
    let retornaSucesso = new responseSuccess;
    let cavaleiro = req.body
    let valorObrigatorio = validarErros.valorObrigatorio(cavaleiro,['nome','patente'],`${server.url}/cadastrar`);
    if(valorObrigatorio.status == null){
        let arrayObrigatorio = validarErros.arrayObrigatorio(cavaleiro,[],`${server.url}/cadastrar`);
        if (arrayObrigatorio.status == null){            
            let novoCavaleiro = new cavaleiros({
                nome: cavaleiro.nome,
                patente: cavaleiro.patente.toLowerCase(),
                descricao: cavaleiro.descricao,
                ataques: cavaleiro.ataques,
                deus: cavaleiro.deus
            })       
            novoCavaleiro.save(function(err,novoCavaleiro){
                if (err) return console.error(err);

                let sucesso = retornaSucesso.cadastradoComSucesso(novoCavaleiro)            
                res.send(200, sucesso);
                
            })                        
        }else{
            res.send(arrayObrigatorio.status, arrayObrigatorio)
        }
    }else{
        res.send(valorObrigatorio.status, valorObrigatorio)
    }
    next();
})

/**
 * @swagger
 * /cavaleiro:
 *   put:
 *     tags:
 *       - cavaleiro
 *     summary: "Usado para atualizar todas as informações de um cavaleiro"
 *     description: ""
 *     parameters: 
 *       - name: x-zodiaco-token
 *         in: header
 *         description: ""
 *         required: true
 *         type: string 
 *       - in: "body"
 *         name: "body"
 *         description: "Parametro obrigatórios para atualização"
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *             nome:
 *               type: string
 *               description: Nome de usuário
 *             patente:
 *               type: string
 *               description: bronze, prata, ouro, etc
 *             descricao:
 *               type: string
 *               description: Senha de acesso
 *             ataques:
 *               type: array
 *               items:
 *                 type: string
 *               description: Ataques utilizado pelo cavaleiro
 *             deus:
 *               type: string
 *               description: Deus protetor do cavaleiro
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Retorna informações atualizadas
 *         schema:
 *           type: object
 *           properties:
 *             success:
 *               type: boolean
 *               description: retorna true quando login é retornado com sucesso
 *             message:
 *               type: string
 *               description: Mensgem informativa
 *             data:
 *               type: object
 *               description: Objeto contendo a chave x-zodiaco-token
 *               properties:
 *                 id:
 *                   type: string
 *                 nome: 
 *                   type: string
 *                 patente:
 *                   type: string
 *                 descricao: 
 *                   type: string
 *                 ataques:
 *                   type: array
 *                   items:
 *                     type: string
 *                 deus: 
 *                   type: string
 */
server.put('/cavaleiro', verificarJWT, function(req,res,next){
    let validarErros = new responseErrors;
    let retornaSucesso = new responseSuccess;    
    let camposObrigatorios = ['id', 'nome', 'patente', 'descricao', 'ataques', 'deus']
    let cavaleiro = {
        id : req.body.id,
        nome : req.body.nome,
        patente : req.body.patente,
        descricao : req.body.descricao,
        ataques : req.body.ataques,
        deus : req.body.deus
    }
    let valorObrigatorio = validarErros.valorObrigatorio(cavaleiro,camposObrigatorios,`${server.url}/cavaleiro`);
    if(valorObrigatorio.status != null){
        res.send(valorObrigatorio.status, valorObrigatorio)
    }else{
        let atualizaCavaleiro = {
            nome : req.body.nome,
            patente : req.body.patente.toLowerCase(),
            descricao : req.body.descricao,
            ataques : req.body.ataques,
            deus : req.body.deus
        }

        cavaleiros.findOneAndUpdate({_id : cavaleiro.id}, atualizaCavaleiro, {upsert: true}, function(err) {
            if (err) return console.error(err)

            return res.send(200, retornaSucesso.dadoAtualizado(atualizaCavaleiro));
        });
    }
});

/**
 * @swagger
 * /cavaleiro:
 *   patch:
 *     tags:
 *       - cavaleiro
 *     summary: "Usado para atualizar  uma ou mais informações de um cavaleiro"
 *     description: ""
 *     parameters: 
 *       - name: x-zodiaco-token
 *         in: header
 *         description: ""
 *         required: true
 *         type: string 
 *       - in: "body"
 *         name: "body"
 *         description: "Parametro necessário para atualização"
 *         required: false
 *         schema:
 *           type: object
 *           properties:
 *             nome:
 *               type: string
 *               description: Nome de usuário
 *             patente:
 *               type: string
 *               description: bronze, prata, ouro, etc
 *             descricao:
 *               type: string
 *               description: Senha de acesso
 *             ataques:
 *               type: array
 *               items:
 *                 type: string
 *               description: Ataques utilizado pelo cavaleiro
 *             deus:
 *               type: string
 *               description: Deus protetor do cavaleiro
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Retorna informações atualizadas
 *         schema:
 *           type: object
 *           properties:
 *             success:
 *               type: boolean
 *               description: retorna true quando login é retornado com sucesso
 *             message:
 *               type: string
 *               description: Mensgem informativa
 *             data:
 *               type: object
 *               description: Objeto contendo a chave x-zodiaco-token
 *               properties:
 *                 id:
 *                   type: string
 *                 nome: 
 *                   type: string
 *                 patente:
 *                   type: string
 *                 descricao: 
 *                   type: string
 *                 ataques:
 *                   type: array
 *                   items:
 *                     type: string
 *                 deus: 
 *                   type: string
 */
server.patch('/cavaleiro', verificarJWT, function(req, res, next){
    let validarErros = new responseErrors;
    let retornaSucesso = new responseSuccess;    
    let camposObrigatorios = ['id']
    
    let valorObrigatorio = validarErros.valorObrigatorio(req.body,camposObrigatorios,`${server.url}/cavaleiro`);

    if(valorObrigatorio.status != null){
        res.send(valorObrigatorio.status, valorObrigatorio)
    }else{
        let query = {_id: req.body.id};
        delete req.body.id
        cavaleiros.updateOne(query, req.body,function(err,success){
            if (err) return console.error(err)
            
            if(!success.nModified){
                let semDadosParaAtualizar =  retornaSucesso.semDadosParaAtualizar();
                return res.send(semDadosParaAtualizar.status, semDadosParaAtualizar);
            }else{
                cavaleiros.findOne(query,function(err,cavaleiro){
                    if (err) return console.error(err)
                    return res.send(200, retornaSucesso.dadoAtualizado(cavaleiro));
                })
            }
            
        })
    }
});

/**
 * @swagger
 * /cavaleiro/id/{id}:
 *   delete:
 *     tags:
 *       - cavaleiro
 *     summary: "Usado para remover um cavaleiro"
 *     description: ""
 *     parameters: 
 *       - name: x-zodiaco-token
 *         in: header
 *         description: ""
 *         required: true
 *         type: string 
 *       - in: "path"
 *         name: "id"
 *         description: "Parametro necessário para exclusão"
 *         required: false
 *         schema:
 *           type: object
 *           properties:
 *             nome:
 *               type: string
 *               description: Nome de usuário
 *             patente:
 *               type: string
 *               description: bronze, prata, ouro, etc
 *             descricao:
 *               type: string
 *               description: Senha de acesso
 *             ataques:
 *               type: array
 *               items:
 *                 type: string
 *               description: Ataques utilizado pelo cavaleiro
 *             deus:
 *               type: string
 *               description: Deus protetor do cavaleiro
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Retorna informações atualizadas
 *         schema:
 *           type: object
 *           properties:
 *             success:
 *               type: boolean
 *               description: retorna true quando login é retornado com sucesso
 *             message:
 *               type: string
 *               description: Mensgem informativa
 *             data:
 *               type: object
 *               description: Objeto contendo a chave x-zodiaco-token
 *               properties:
 *                 id:
 *                   type: string
 *                 nome: 
 *                   type: string
 *                 patente:
 *                   type: string
 *                 descricao: 
 *                   type: string
 *                 ataques:
 *                   type: array
 *                   items:
 *                     type: string
 *                 deus: 
 *                   type: string
 */
server.del('/cavaleiro/id/:id', verificarJWT, function(req, res, next){
    let validarErros = new responseErrors;
    let retornaSucesso = new responseSuccess;    
    let camposObrigatorios = ['id']

    let valorObrigatorio = validarErros.valorObrigatorio(req.params,camposObrigatorios,`${server.url}/cavaleiro`);

    if(valorObrigatorio.status != null){
        res.send(valorObrigatorio.status, valorObrigatorio)
    }else{
        // console.log(valorObrigatorio)
        let query = { _id: req.params.id }
        cavaleiros.findOne(query,function(err,cavaleiro){
            if (err) return console.error(err)
            
            /** Cavaleiro não localizado */
            if(cavaleiro == null) return res.send(400, retornaSucesso.dadosNaoEncontrados())
                
            cavaleiro.remove(query,function(){                
                let dadoRemovido = retornaSucesso.dadoRemovido(cavaleiro);
                res.send(200, dadoRemovido)
            })
          
        })
    }
})

/**
 * @swagger
 * /cavaleiro/id/{id}:
 *   get:
 *     tags:
 *       - cavaleiro
 *     summary: "Usado para consultar um cavaleiro"
 *     description: ""
 *     parameters: 
 *       - name: x-zodiaco-token
 *         in: header
 *         description: ""
 *         required: true
 *         type: string 
 *       - in: "path"
 *         name: "id"
 *         description: "Parametro necessário para consulta"
 *         required: false
 *         schema:
 *           type: object
 *           properties:
 *             nome:
 *               type: string
 *               description: Nome de usuário
 *             patente:
 *               type: string
 *               description: bronze, prata, ouro, etc
 *             descricao:
 *               type: string
 *               description: Senha de acesso
 *             ataques:
 *               type: array
 *               items:
 *                 type: string
 *               description: Ataques utilizado pelo cavaleiro
 *             deus:
 *               type: string
 *               description: Deus protetor do cavaleiro
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Retorna informações atualizadas
 *         schema:
 *           type: object
 *           properties:
 *             success:
 *               type: boolean
 *               description: retorna true quando login é retornado com sucesso
 *             message:
 *               type: string
 *               description: Mensgem informativa
 *             data:
 *               type: object
 *               description: Objeto contendo a chave x-zodiaco-token
 *               properties:
 *                 id:
 *                   type: string
 *                 nome: 
 *                   type: string
 *                 patente:
 *                   type: string
 *                 descricao: 
 *                   type: string
 *                 ataques:
 *                   type: array
 *                   items:
 *                     type: string
 *                 deus: 
 *                   type: string
 */
server.get('/cavaleiro/id/:id', verificarJWT, function(req,res,next){     
    let validarErros = new responseErrors;
    let retornaSucesso = new responseSuccess;
    let valorObrigatorio = validarErros.valorObrigatorio(req.params,['id'],'/cavaleiro/id/');
    if(valorObrigatorio.status != null){
        res.send(valorObrigatorio.status, valorObrigatorio)
        next() 
    }else{      
        cavaleiros.findById(req.params.id,function(err,cavaleiro){
            if(err){
                return console.error(err);
            }         
            if(cavaleiro == null || cavaleiro.length == 0){
                res.send(200, retornaSucesso.dadosNaoEncontrados());
            }else{
                let sucesso = retornaSucesso.dadoEncontrado(cavaleiro);
                res.send(200, sucesso); 
            }
        })
        next();
    }

    
})

server.get('/cavaleiros', function(req,res,next){     
    let validarErros = new responseErrors;
    let retornaSucesso = new responseSuccess;
    cavaleiros.find({}, function(err, cavaleiro) {
        let sucesso = retornaSucesso.dadosEncontrados(cavaleiro);        
        res.send(200, sucesso); 
    });

    next();   
})

/**
 * @swagger
 * /cavaleiro/patente/{patente}:
 *   get:
 *     tags:
 *       - cavaleiro
 *     summary: "Usado para consultar cavaleiros por sua patente"
 *     description: ""
 *     parameters: 
 *       - name: x-zodiaco-token
 *         in: header
 *         description: ""
 *         required: true
 *         type: string 
 *       - in: "path"
 *         name: "patente"
 *         description: "Bronze, Prata ou Ouro"
 *         type:
 *         enum:
 *         - Bronze
 *         - Prata
 *         - Ouro
 *         required: true
 *         schema:
 *           type: object
 *           properties:
 *             nome:
 *               type: string
 *               description: Nome de usuário
 *             patente:
 *               type: string
 *               description: bronze, prata, ouro, etc
 *             descricao:
 *               type: string
 *               description: Senha de acesso
 *             ataques:
 *               type: array
 *               items:
 *                 type: string
 *               description: Ataques utilizado pelo cavaleiro
 *             deus:
 *               type: string
 *               description: Deus protetor do cavaleiro
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Retorna informações atualizadas
 *         schema:
 *           type: object
 *           properties:
 *             success:
 *               type: boolean
 *               description: retorna true quando login é retornado com sucesso
 *             message:
 *               type: string
 *               description: Mensgem informativa
 *             data:
 *               type: object
 *               description: Objeto contendo a chave x-zodiaco-token
 *               properties:
 *                 id:
 *                   type: string
 *                 nome: 
 *                   type: string
 *                 patente:
 *                   type: string
 *                 descricao: 
 *                   type: string
 *                 ataques:
 *                   type: array
 *                   items:
 *                     type: string
 *                 deus: 
 *                   type: string
 */
server.get('/cavaleiro/patente/:patente', verificarJWT, function(req,res,next){    
    let validarErros = new responseErrors;
    let retornaSucesso = new responseSuccess;
    let valorObrigatorio = validarErros.valorObrigatorio(req.params,['patente'],'/cavaleiro/patente/');
    if(valorObrigatorio.status != null){
        res.send(valorObrigatorio.status, valorObrigatorio)
        next() 
    }else{    
        let patente = req.params.patente.toLowerCase();
        cavaleiros.find( {patente: patente},function(err,cavaleiro){
            if(err){
                console.error(err);
            }
            if(cavaleiro == null || cavaleiro.length == 0){
                res.send(200, retornaSucesso.dadosNaoEncontrados());
            }else{
                let sucesso = retornaSucesso.dadosEncontrados(cavaleiro);
                res.send(200, sucesso); 
            }
        })
    }
    
});

/**
 * @swagger
 * /cavaleiro:
 *   get:
 *     tags:
 *       - cavaleiro
 *     summary: "Usado para consultar um cavaleiro por nome"
 *     description: ""
 *     parameters: 
 *       - name: x-zodiaco-token
 *         in: header
 *         description: ""
 *         required: true
 *         type: string
 *       - name: "nome"
 *         in: query
 *         description: ""
 *         required: true
 *         type: string
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Retorna resultado da consulta
 *         schema:
 *           type: object
 *           properties:
 *             success:
 *               type: boolean
 *               description: retorna true quando login é retornado com sucesso
 *             message:
 *               type: string
 *               description: Mensgem informativa
 *             data:
 *               type: object
 *               description: Objeto contendo a chave x-zodiaco-token
 *               properties:
 *                 nome: 
 *                   type: string
 *                 patente:
 *                   type: string
 *                 descricao: 
 *                   type: string
 *                 ataques:
 *                   type: array
 *                   items:
 *                     type: string
 *                 deus: 
 *                   type: string
 */
server.get('/cavaleiro', verificarJWT, function(req,res, next){    
    let validarErros = new responseErrors;
    let retornaSucesso = new responseSuccess;
    let valorObrigatorio = validarErros.valorObrigatorio(req.query,['nome'],'/cavaleiro');
    if(valorObrigatorio.status != null){
        res.send(valorObrigatorio.status, valorObrigatorio)
        next() 
    }else{   
        cavaleiros.find({nome: new RegExp(req.query.nome, 'i')},function(err,cavaleiro){
            if(err){
                return console.error(err);
            }
            if(cavaleiro == null || cavaleiro.length == 0){
                res.send(200, retornaSucesso.dadosNaoEncontrados());
            }else{
                let sucesso = retornaSucesso.dadosEncontrados(cavaleiro);
                res.send(200, sucesso);
            }
        })
    }
    
})

/** O swagger está aqui */
server.get(/\/api\-docs\/?.*/, restify.plugins.serveStatic({
    directory: './public',
    default: 'index.html'
}));
  
/** O front está aqui */
server.get(/\/front\/?.*/, restify.plugins.serveStatic({   
    directory: './public',
    default: 'index.html'
}));

server.listen(process.env.API_PORT, function(){
    console.log('%s escutando em  %s', server.name, server.url);
});

module.exports = server